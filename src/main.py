import requests
import sqlite3
import re
from bs4 import BeautifulSoup
from multiprocessing import Process

urls_db = sqlite3.connect('seed_urls.db')
urls_cursor = urls_db.cursor()
seed_urls = ()


def get_seed_urls():
    urls_cursor.execute('SELECT * FROM urls')
    global seed_urls
    seed_urls = urls_cursor.fetchall()
    return seed_urls


def get(url_to_get: str):
    headers = {
        'User-Agent': 'Test-search-engine/0.0.1',
    }

    response = requests.get(url_to_get, headers=headers)
    return response


def walk_in_site(url: str):
    try:
        sitemap = get(f'{url}/sitemap.xml')
        sitemap = sitemap.text

        map_soup: BeautifulSoup = BeautifulSoup(sitemap, 'xml')
        locations = map_soup.find_all('loc')
    except:
        homepage = get(url)
        homepage = homepage.text
        homepage_soup: BeautifulSoup = BeautifulSoup(homepage, 'xml')
        locations = homepage_soup.find_all('loc')

    for location in locations:
        location = location.getText()
        yield location
        try:
            page = requests.get(location)
            page = page.text
            page_soup = BeautifulSoup(page, 'html.parser')
            more_locations = page_soup.find_all('a', href=True)
            more_locations = more_locations
            for more_location in more_locations:
                more_location = more_location['href']
                # If the link is a relative link
                if more_location[0] == '.' or more_location[0] == '/':
                    more_location = location + more_location
                yield walk_in_site(more_location)
        except:
            pass


def search_in_page(text: str, search_text: str):
    result = re.search(search_text, text)
    if result is not None:
        return True
    else:
        return False


def do_full_search(page: str, search_test: str):
    try:
        page_text = get(page).text
        useful_page = search_in_page(page_text, search_test)
        if useful_page:
            print(f'>> Useful Page: {page}')
        else:
            print(f'>> Useless Page: {page}')
            print()
    except:
        pass


if __name__ == '__main__':
    seed_urls = get_seed_urls()

    text = input('Search: ')

    for url in seed_urls:
        for current_page in walk_in_site(url[0]):
            search = Process(target=do_full_search, args=(current_page, text))
            search.start()

    urls_db.close()
